Exercise 04
===========

Welcome to Markdown!
<p> Drishen Naidoo </p>

Markdown is a text-to-HTML conversion tool for web writers. 
Markdown allows you to write using an easy-to-read, easy-to-write 
plain text format, then convert it to structurally valid HTML.

Favorite Things
===============

These are a few of my favorite things:
<ul>
<li>Raindrops on roses</li>
 <li>Whiskers on kittens</li>
  <li>Bright copper kettles</li>
   <li>Warm woolen mittens</li>
<li>Brown paper packages tied up with strings</li>
 <li>Cream-colored ponies</li>
  <li>Crisp apple strudels</li>
<li>Doorbells and sleigh bells</li>
 <li>Schnitzel with noodles</li>
  <li>Wild geese that fly with the moon on their wings</li>
<li>Girls in white dresses with blue satin sashes</li>
 <li>Snowflakes that stay on my nose and eyelashes</li>
  <li>Silver-white winters that melt into springs</li>
</ul>
